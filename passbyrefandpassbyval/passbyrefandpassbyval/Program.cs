﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace passbyrefandpassbyval
{
    class Program
    {
        static void Main(string[] args)
        {
            Get p = new Get();
            int t = p.p = 10;
            Console.WriteLine(p.p.ToString());
            change(ref t);
            Console.WriteLine(t.ToString());
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Nairitya Khilari");
            Console.ReadLine();
        }
        class Get
        {
            public int p { get; set; }
        }

        private static void change(ref int t)
        {
            t = 34;
        }
    }
}