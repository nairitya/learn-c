﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using My_Schedule.Resources;
using System.Diagnostics;
using System.IO;
namespace My_Schedule
{
    public partial class MainPage : PhoneApplicationPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        public void start(object sender, RoutedEventArgs e)
        {
            authHttp();
        }

        public void authHttp()
        {
            string avatarUri = "http://sleepy-plains-8147.herokuapp.com/";
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(avatarUri);
            request.BeginGetResponse(new AsyncCallback(ReadWebRequestCallback), request);
        }

        private void ReadWebRequestCallback(IAsyncResult callbackResult)
        {
            HttpWebRequest myRequest = (HttpWebRequest)callbackResult.AsyncState;
            HttpWebResponse myResponse = (HttpWebResponse)myRequest.EndGetResponse(callbackResult);
            using (StreamReader httpwebStreamReader = new StreamReader(myResponse.GetResponseStream()))
            {
                string results = httpwebStreamReader.ReadToEnd();
            }
            myResponse.Close();
            Exit_Tap();
        }

        public void about(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/About.xaml", UriKind.Relative));
        }

        public void Exit_Tap()
        {
            Application.Current.Terminate();
        }
    }
}