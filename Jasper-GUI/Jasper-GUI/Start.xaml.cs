﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

using System.Globalization;
using Microsoft.Phone.Globalization;
using System.Collections.ObjectModel;
using Microsoft.Phone.UserData;

namespace Jasper_GUI
{
    public partial class Start : PhoneApplicationPage
    {
        public Start()
        {
            InitializeComponent();

            List<AddressBook> source = new List<AddressBook>();
            string p = "127.17.15.15";
            string pl = "Nairitya Khilari";
            /*
            string phone = "8439591486";
            string country = "India";
             * */
            source.Add(new AddressBook(p,pl));
            List<string> authors = new List<string>();
            authors.Add("127.17.15.16");
            authors.Add("127.17.15.17");
            authors.Add("127.17.15.18");
            authors.Add("127.17.15.19");
            authors.Add("127.17.15.40");
            foreach (string q in authors)
            {
                source.Add(new AddressBook(q,pl));
            }
            List<AlphaKeyGroup<AddressBook>> DataSource = AlphaKeyGroup<AddressBook>.CreateGroups(source,
                System.Threading.Thread.CurrentThread.CurrentUICulture,
                (AddressBook s) => { return s.LastName; }, true);
            AddrBook.ItemsSource = DataSource;

        }
    }

    public class AddressBook
    {
        public string FirstName
        {
            get;
            set;
        }
        public string LastName
        {
            get;
            set;
        }
        /*
        public string Address
        {
            get;
            set;
        }
        public string Phone
        {
            get;
            set;
        }
        
        public AddressBook(string firstname, string lastname, string address, string phone)
        */
        public AddressBook(string firstname, string lastname)
        {
            this.FirstName = firstname;
            this.LastName = lastname;
            /*
            this.Address = address;
            this.Phone = phone;
             * */
        }
    }
}