﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Jasper_GUI.Resources;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.IsolatedStorage;
using System.Collections;

using System.Globalization;
using Microsoft.Phone.Globalization;
namespace Jasper_GUI
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();
        }
        public void About(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/About.xaml", UriKind.Relative));
        }
        public void Credits(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Credits.xaml", UriKind.Relative));
        }
        public void Start(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Start.xaml", UriKind.Relative));
        }
        private void Exit(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine(e.ToString());
            throw new ExitException();
        }
    }
}