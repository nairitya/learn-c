﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace @string
{
    class Program
    {
        static void Main(string[] args)
        {
            string t = "The Quiter you become The more you will be able to learn";
            Console.WriteLine(t.ToLower());
            Console.WriteLine(t.ToUpper());
            Console.WriteLine(t.Replace("Quiter", "Nkman"));
            //t.Append("OKay ?") Use StringBuilder for this, not in need right now;
            Console.WriteLine(t);
            Console.ReadLine();
        }
    }
}
