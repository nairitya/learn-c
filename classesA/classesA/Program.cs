﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace classesA
{
    class Program
    {
        static void Main(string[] args)
        {
            Student me = new Student();
            me.name = "Nairitya Khilari";
            me.sex = "Male";
            me.age = 19;
            me.department = "CSE";
            Console.WriteLine("{0} - {1} - {2} - {3}", me.name, me.sex, me.age, me.department);
            Console.WriteLine(me.who());
            Console.ReadLine(); 
        }

        class Student
        {
            public string name { set; get; }
            public string sex { get; set; }
            public int age { get; set; }
            public string department { get; set; }
            public string who()
            { 
                string st = "Hello " + this.name + " You Must Be NKMAN ? Aren't You ?";
                return st;
            }
        }
    }
}
