﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace timeanddate
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime mint = DateTime.Now;
            Console.WriteLine(mint.ToString());
            Console.WriteLine(mint.ToShortTimeString());
            Console.WriteLine(mint.ToShortDateString());
            Console.WriteLine(mint.Year);
            DateTime bday = new DateTime(1994,12,18);
            TimeSpan age = DateTime.Now.Subtract(bday);
            Console.WriteLine(age.TotalDays.ToString());
            Console.ReadLine();
        }
    }
}
